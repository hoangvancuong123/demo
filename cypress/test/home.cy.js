import HomePage from "../src/page/homePage"
import HomePageLocator from "../src/locator/homePageLocator"
import {textValue} from "../src/constant/textValue";

const homePage = new HomePage()

describe("Testing home page", () => {
    beforeEach("Navigate to home page", () => {
        homePage.navigateToHomePage()
    })

    it("Should open the home page", () => {
        cy.get(HomePageLocator.homePageTitle)
            .should("have.text", textValue.pageTitle)
        cy.get(HomePageLocator.btnMakeAppointment)
            .should("be.visible")
            .and("have.text", textValue.btnMakeAppointmentText)
    })
})