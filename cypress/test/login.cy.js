import LoginPage from "../src/page/loginPage"
import LOGIN_PAGE_LOCATOR from "../src/locator/loginPageLocator"
import MAKE_APPOINTMENT_LOCATOR from "../src/locator/makeAppointmentPageLocator"
import signUpData from '../fixtures/dataLogin.json'
import {textValue} from "../src/constant/textValue";

const loginPage = new LoginPage()

describe("Testing login page", () => {

    beforeEach("Navigate to home page", () => {
     loginPage.navigateToLoginPage()
    })

    it("should login successfully", () => {
        loginPage.inputUsername(signUpData.validUsername)
            .inputPassword(signUpData.validPassword)
            .clickButtonLogin()

        cy.get(MAKE_APPOINTMENT_LOCATOR.formInput)
            .should("be.visible")

        cy.get(MAKE_APPOINTMENT_LOCATOR.btnBookAppointment)
            .should("be.visible")
            .and("have.text", textValue.btmBookAppointmentText)
    })

    it("should login failed due to invalid username", () => {
        loginPage.inputUsername(signUpData.invalidUsername)
            .inputPassword(signUpData.validPassword)
            .clickButtonLogin()

        cy.get(LOGIN_PAGE_LOCATOR.txtLoginFailed)
            .should("have.text", textValue.loginFailedText)
    })

    it("should login failed due to invalid password", () => {
        loginPage.inputUsername(signUpData.validUsername)
            .inputPassword(signUpData.invalidPassword)
            .clickButtonLogin()

        cy.get(LOGIN_PAGE_LOCATOR.txtLoginFailed)
            .should("have.text", textValue.loginFailedText)
    })

})