const LoginPageLocator = {
    inpUsername: "#txt-username",
    inpPassword: "#txt-password",
    btnLogin: "#btn-login",
    txtLoginFailed: ".lead.text-danger"
}

module.exports = LoginPageLocator