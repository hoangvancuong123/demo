const HomePageLocator = {
    btnMakeAppointment: "#btn-make-appointment",
    menuIcon: ".fa.fa-bars",
    homePageTitle: "h1"
}

module.exports = HomePageLocator