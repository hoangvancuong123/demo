const makeAppointmentPageLocator = {
    formInput: ".form-horizontal",
    btnBookAppointment: "#btn-book-appointment"
}

module.exports = makeAppointmentPageLocator