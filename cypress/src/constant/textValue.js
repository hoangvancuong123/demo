export const textValue = {
    "pageTitle": "CURA Healthcare Service",
    "btnMakeAppointmentText": "Make Appointment",
    "btmBookAppointmentText": "Book Appointment",
    "loginFailedText": "Login failed! Please ensure the username and password are valid."
}