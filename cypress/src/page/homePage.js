import HOME_PAGE_LOCATOR from "../locator/homePageLocator"

class HomePage {
    navigateToHomePage() {
        cy.visit("/")
    }

    clickButtonMakeAppointment() {
        cy.get(HOME_PAGE_LOCATOR.btnMakeAppointment).click()
    }

    clickMenuIcon() {
        cy.get(HOME_PAGE_LOCATOR.menuIcon).click()
    }
}

module.exports = HomePage