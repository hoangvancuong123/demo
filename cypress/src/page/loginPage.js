import LOGIN_PAGE_LOCATOR from "../locator/loginPageLocator"

class LoginPage {
    navigateToLoginPage() {
        cy.visit("/profile.php#login")
        return this
    }

    inputUsername(username) {
        cy.get(LOGIN_PAGE_LOCATOR.inpUsername)
            .type(username)
        return this
    }

    inputPassword(password) {
        cy.get(LOGIN_PAGE_LOCATOR.inpPassword)
            .type(password)
        return this
    }

    clickButtonLogin() {
        cy.get(LOGIN_PAGE_LOCATOR.btnLogin).click()
    }
}

module.exports = LoginPage