const { defineConfig } = require("cypress");

module.exports = defineConfig({
    e2e: {
        setupNodeEvents(on, config) {
        },
        specPattern: "./cypress/test/**.*",
        baseUrl: "https://katalon-demo-cura.herokuapp.com/"
    },
});
